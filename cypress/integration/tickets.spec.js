describe('Tickets', () => {
    //Executa antes de cada teste 
    //cy.visit -> Acessa URLs   
    beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'))

    it("fills all the text input fields", () => {
        //variaveis
        const firstName = "Edson"
        const lastName = "Costa"

        //cy.get -> Pega o elemento 
        // .type -> Digita
        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastName)
        cy.get("#email").type("edsonlimajll@gmail.com")
        cy.get("#requests").type("Churrasco")
        cy.get("#signature").type(`${firstName} ${lastName}`)
    })

    it("select two tickets", () => {
        // .select seleciona um array dentro de um combo
        cy.get("#ticket-quantity").select(1);
    })

    it("select 'vip' ticket type", () => {
        // .check -> seleciona radio button
        cy.get("#vip").check();
    })

    it("selects 'social media' checkbox", () => {
        cy.get("#social-media").check()
    })

    it("selects 'friend', and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check()
        cy.get("#publication").check()
        cy.get("#friend").uncheck()
    })

    it('has "TICKETBOX" headers heading', () => {
        //verifica um texto dentro de um header / h1 contendo texto
        cy.get("header h1").should("contain", "TICKETBOX")
    })

    it("alerts on invalid email", () => {
        cy.get("#email")
            .as("email")
            .type("edsonlimajll-.gmail.com")

        //vai validar a classe invalid se existe
        cy.get("#email.invalid")
            .should("exist")

        cy.get("@email")
            .clear()
            .type("edsonlimajll@gmail.com")

        cy.get("email.invalid").should("not.exist")

    })

    it("fills and reset the form", () => {
        const firstName = "Edson"
        const lastName = "Costa"
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastName)
        cy.get("#email").type("edsonlimajll@gmail.com")
        cy.get("#ticket-quantity").select(1)
        cy.get("#vip").check();
        cy.get("#friend").check()
        cy.get("#requests").type("Churrasco")

        cy.get(".agreement p")
            .should("contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`)

        cy.get("#agree").click()

        cy.get("#signature").type(`${fullName}`)

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")

        cy.get("button[type='reset']").click()

        cy.get("@submitButton").should("be.disabled")

    })

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "Edson",
            lastName: "Lima",
            email: "edsonlimajll@gmail.com"
        }

        cy.fillMandatoryFields(customer)

        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled")

        cy.get("#agree").uncheck()

        cy.get("@submitButton").should("be.disabled")

    })

})